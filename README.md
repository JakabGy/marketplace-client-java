# marketplace-client-java

This library provides a Java wrapper for a subset of the [Atlassian Marketplace](https://marketplace.atlassian.com) REST API.

Requirements:

* Java 8 or above
* Third-party library dependencies as listed in `pom.xml` (or use the all-in-one jar as described below); these include several Apache Commons libraries, GSON, Guava, Joda-Time, and SLF4j

For detailed change history and examples, see `changelog.md` in this project; the [`marketplace-v2-api-tutorials`](https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials) code; and the JIRA project (see below).

## Obtaining and using the library

The library jar is available from Maven Central:

    <dependency>
        <groupId>com.atlassian.marketplace</groupId>
        <artifactId>marketplace-client-java</artifactId>
        <version>2.0.0</version> <!-- replace with latest release version -->
    </dependency>

There is also an all-in-one jar that includes all of the third-party dependencies:

    <dependency>
        <groupId>com.atlassian.marketplace</groupId>
        <artifactId>marketplace-client-java</artifactId>
        <version>2.0.0</version> <!-- replace with latest release version -->
        <classifier>dependencies</classifier>
    </dependency>

The entry point for all API methods is the `MarketplaceClient` class, which can be instantiated and configured using `MarketplaceClientFactory`. A client instance with no additional configuration provides access to the same data that can be viewed publicly on the Marketplace site; if you want to query data that is only visible to a vendor or administrator, or post new data to the server (such as publishing a new add-on version listing), you can configure the client with your Atlassian Account credentials.

For details on all available API methods, see the [Javadoc](https://docs.atlassian.com/marketplace-client-java/), as well as the [Marketplace API](https://developer.atlassian.com/display/MARKET/Marketplace+API) section on [https://developer.atlassian.com](developer.atlassian.com). 

## Contributing and reporting issues

Issues and releases are tracked in JIRA, in the [MPJC project](https://ecosystem.atlassian.net/projects/MPJC/summary) on ecosystem.atlassian.net.

Pull requests are welcome. The project requires Maven 3 to build; we recommend Maven 3.3.9.

## License

This project is released under the [Apache 2 license](https://bitbucket.org/atlassian-marketplace/marketplace-client-java/src/master/LICENSE.txt).

##Releasing

There is a [bamboo job](https://ecosystem-bamboo.internal.atlassian.com/browse/UPM-MPACCLIENTRELEASE) responsible for releasing a new version of the client.
To release a new version, run the job with the values of these custom variables set:
* mpcj.maven.release.version (usually the same as the current version in the POM).
* mpcj.maven.development.version (usually one more than the current version).
