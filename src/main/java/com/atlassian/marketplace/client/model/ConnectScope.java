package com.atlassian.marketplace.client.model;

import java.net.URI;

/**
 * Information about an Atlassian Connect scope that is used by an {@link AddonVersion}.
 * @since 2.0.0
 */
public class ConnectScope
{
    Links _links;
    @RequiredLink(rel = "alternate") URI alternateUri;
    
    String key;
    String name;
    String description;

    public Links getLinks()
    {
        return _links;
    }

    /**
     * Address of the web page describing this scope.
     */
    public URI getAlternateUri()
    {
        return alternateUri;
    }
    
    /**
     * The scope key.
     */
    public String getKey()
    {
        return key;
    }

    /**
     * The scope name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * The scope description.
     */
    public String getDescription()
    {
        return description;
    }
}
