package com.atlassian.marketplace.client.api;

import io.atlassian.fugue.Either;

import java.util.Optional;

import static com.atlassian.marketplace.client.util.Convert.iterableOf;
import static com.atlassian.marketplace.client.util.Convert.toOptional;

/**
 * Encapsulates parameters that can be passed to {@link Products#safeGetVersion}.
 * @since 2.0.0
 */
public final class ProductVersionSpecifier
{
    private final Optional<Either<Integer, String>> nameOrBuild;
    
    private ProductVersionSpecifier(Optional<Either<Integer, String>> nameOrBuild)
    {
        this.nameOrBuild = nameOrBuild;
    }
    
    /**
     * Searches for a product version by build number.
     */
    public static ProductVersionSpecifier buildNumber(int buildNumber)
    {
        return new ProductVersionSpecifier(Optional.of(Either.left(buildNumber)));
    }

    /**
     * Searches for a product version by name (version string).
     */
    public static ProductVersionSpecifier name(String name)
    {
        return new ProductVersionSpecifier(Optional.of(Either.right(name)));
    }

    /**
     * Searches for the latest version.
     */
    public static ProductVersionSpecifier latest()
    {
        return new ProductVersionSpecifier(Optional.empty());
    }

    public Optional<Integer> safeGetBuildNumber(){
        for (Either<Integer, String> nb: iterableOf(nameOrBuild))
        {
            return toOptional(nb.left().toOption());
        }
        return Optional.empty();
    }

    public Optional<String> safeGetName()
    {
        for (Either<Integer, String> nb: iterableOf(nameOrBuild))
        {
            return toOptional(nb.right().toOption());
        }
        return Optional.empty();
    }

    @Override
    public String toString()
    {
        for (Integer b: iterableOf(safeGetBuildNumber()))
        {
            return "buildNumber(" + b + ")";            
        }
        for (String n: iterableOf(safeGetName()))
        {
            return "name(" + n + ")";
        }
        return "latest";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ProductVersionSpecifier) && ((ProductVersionSpecifier) other).nameOrBuild.equals(this.nameOrBuild);
    }
    
    @Override
    public int hashCode()
    {
        return nameOrBuild.hashCode();
    }
}