package com.atlassian.marketplace.client.api;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Optional;

import static com.atlassian.marketplace.client.util.Convert.iterableOf;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Interfaces for common query builder criteria, allowing classes like {@link AddonQuery.Builder} to be treated abstractly.
 * @since 2.0.0
 */
public abstract class QueryBuilderProperties
{
    private QueryBuilderProperties()
    {
    }
    
    /**
     * Common interface for query builders that have an "access token" parameter.
     * @see QueryProperties.AccessToken
     */
    public interface AccessToken<T extends AccessToken<T>>
    {
        /**
         * Optionally includes an access token in the query, allowing access to any private add-on
         * that has such a token.
         * @param accessToken an access token string, or {@link Optional#empty}
         * @return  the same query builder
         * @see QueryProperties.AccessToken#safeGetAccessToken()
         */
        T accessToken(Optional<String> accessToken);
    }

    /**
     * Common interface for query builders that have "application" and "application build number" parameters.
     * @see QueryProperties.ApplicationCriteria
     */
    public interface ApplicationCriteria<T extends ApplicationCriteria<T>>
    {
        /**
         * Restricts the query to add-ons that are compatible with the specified application.
         * @param application  an {@link ApplicationKey}, or {@link Optional#empty} for no application filter
         * @return  the same query builder
         * @see QueryBuilderProperties.ApplicationCriteria#application(Optional)
         */
        T application(Optional<ApplicationKey> application);

        /**
         * Restricts the query to add-ons that are compatible with the specified application version.
         * This is ignored if you have not specified {@link #application}.
         * @param appBuildNumber  the application build number, or {@link Optional#empty} for no application
         *   version filter
         * @return  the same query builder
         * @see QueryBuilderProperties.ApplicationCriteria#appBuildNumber(Optional)
         */
        T appBuildNumber(Optional<Integer> appBuildNumber);
    }

    /**
     * Common interface for query builders that have "offset" and "limit" parameters.
     */
    public interface Bounds<T extends Bounds<T>>
    {
        /**
         * Sets the starting offset and/or the maximum result page size for the query, from a
         * {@link QueryBounds} instance.
         * @param bounds  a {@link QueryBounds} specifying an offset and/or limit
         * @return  the same query builder
         */
        T bounds(QueryBounds bounds);
    }

    /**
     * Common interface for query builders that have a "cost" parameter.
     */
    public interface Cost<T extends Cost<T>>
    {
        /**
         * Restricts the query to add-ons/products that match the given {@link com.atlassian.marketplace.client.api.Cost} criteria.
         * @param cost  a {@link com.atlassian.marketplace.client.api.Cost} value, or {@link Optional#empty} for no cost filter
         * @return  the same query builder
         */
        T cost(Optional<com.atlassian.marketplace.client.api.Cost> cost);
    }
    
    /**
     * Common interface for query builders that have a "hosting" parameter.
     */
    public interface Hosting<T extends Hosting<T>>
    {
        /**
         * Restricts the query to add-ons that support the given hosting type.
         * @param hosting  a {@link HostingType} constant, or {@link Optional#empty} for no hosting filter
         * @return  the same query builder
         */
        T hosting(Optional<HostingType> hosting);
    }

    /**
     * Common interface for query builders that have a multi-valued "hosting" parameter.
     * @since 2.1.0
     */
    public interface MultiHosting<T extends MultiHosting<T>>
    {
        /**
         * Restricts the query to add-ons that support the given hosting types.
         * @param hostings  a list of {@link HostingType} constants, or an empty list for no hosting filter
         * @return  the same query builder
         */
        T hosting(List<HostingType> hostings);
    }

    /**
     * Common interface for query builders that have an "includePrivate" parameter.
     */
    public interface IncludePrivate<T extends IncludePrivate<T>>
    {
        /**
         * Includes private results in the query results.
         * @param includePrivate whether to include private entities in the query results
         * @return the same query builder
         */
        T includePrivate(boolean includePrivate);
    }

    /**
     * Common interface for query builders that have a "withVersion" parameter.
     */
    public interface WithVersion<T extends WithVersion<T>>
    {
        /**
         * Specifies whether the result set should include version-level properties.  This is false by default.
         * @param withVersion  true if the result set should include version data
         * @return  the same query builder
         */
        T withVersion(boolean withVersion);
    }
    
    // Helper classes beyond this point are used internally to simplify query builder implementations.

    static class ApplicationCriteriaHelper
    {
        public final Optional<ApplicationKey> application;
        public final Optional<Integer> appBuildNumber;

        public ApplicationCriteriaHelper()
        {
            this(Optional.empty(), Optional.empty());
        }

        private ApplicationCriteriaHelper(Optional<ApplicationKey> application, Optional<Integer> appBuildNumber)
        {
            this.application = application;
            this.appBuildNumber = appBuildNumber;
        }

        public ApplicationCriteriaHelper application(Optional<ApplicationKey> application)
        {
            return new ApplicationCriteriaHelper(checkNotNull(application), this.appBuildNumber);
        }

        public ApplicationCriteriaHelper appBuildNumber(Optional<Integer> appBuildNumber)
        {
            return new ApplicationCriteriaHelper(this.application, checkNotNull(appBuildNumber));
        }

        public Iterable<String> describe()
        {
            ImmutableList.Builder<String> ret = ImmutableList.builder();
            for (ApplicationKey a: iterableOf(application))
            {
                ret.add("application(" + a.getKey() + ")");
            }
            for (Integer ab: iterableOf(appBuildNumber))
            {
                ret.add("appBuildNumber(" + ab + ")");
            }
            return ret.build();
        }
    }
}
