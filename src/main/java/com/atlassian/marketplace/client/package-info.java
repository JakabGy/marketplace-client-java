/**
 * Top-level interfaces and value classes for the Atlassian Marketplace API.
 */
package com.atlassian.marketplace.client;