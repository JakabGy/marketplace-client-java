package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.LicenseTypes;
import com.atlassian.marketplace.client.model.LicenseType;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableList;

import java.util.Optional;
import java.util.stream.StreamSupport;

final class LicenseTypesImpl implements LicenseTypes
{
    private final ApiHelper apiHelper;
    private final InternalModel.MinimalLinks root;

    LicenseTypesImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root)
    {
        this.apiHelper = apiHelper;
        this.root = root;
    }

    @Override
    public Iterable<LicenseType> getAllLicenseTypes() throws MpacException
    {
        UriBuilder uri = licenseTypesBaseUri();
        InternalModel.LicenseTypes collectionRep =
            apiHelper.getEntity(uri.build(), InternalModel.LicenseTypes.class);
        return ImmutableList.copyOf(collectionRep.getItems());
    }

    @Override
    public Optional<LicenseType> safeGetByKey(final String licenseTypeKey) throws MpacException {
        return StreamSupport.stream(getAllLicenseTypes().spliterator(), false)
                .filter(l -> l.getKey().equals(licenseTypeKey))
                .findFirst();
    }

    private UriBuilder licenseTypesBaseUri() throws MpacException
    {
        return UriBuilder.fromUri(apiHelper.requireLinkUri(root.getLinks(), "licenseTypes", root.getClass()));
    }
}
