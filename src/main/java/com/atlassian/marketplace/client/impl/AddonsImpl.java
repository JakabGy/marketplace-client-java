package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonQuery;
import com.atlassian.marketplace.client.api.AddonVersionSpecifier;
import com.atlassian.marketplace.client.api.AddonVersionsQuery;
import com.atlassian.marketplace.client.api.Addons;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.PricingType;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.http.SimpleHttpResponse;
import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonPricing;
import com.atlassian.marketplace.client.model.AddonReference;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionSummary;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;
import org.apache.commons.io.input.NullInputStream;
import org.apache.http.client.methods.HttpPost;

import java.net.URI;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.atlassian.marketplace.client.impl.ApiHelper.JSON;
import static com.atlassian.marketplace.client.impl.ApiHelper.getTemplatedLink;
import static com.atlassian.marketplace.client.impl.ApiHelper.withAccessToken;
import static com.atlassian.marketplace.client.impl.ApiHelper.withZeroLimit;
import static com.atlassian.marketplace.client.util.Convert.iterableOf;

final class AddonsImpl extends ApiImplBase implements Addons
{

    private static final String DATA_CENTER_BUILD_NUMBER_HEADER_NAME = "X-Mpac-DataCenter-BuildNumber";

    AddonsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "addons");
    }

    @Override
    public Optional<Addon> safeGetByKey(String addonKey, AddonQuery query) throws MpacException
    {
        InternalModel.Addons collectionRep = getEmptyBaseCollectionRep();
        UriTemplate byKeyTemplate = ApiHelper.requireLinkUriTemplate(collectionRep.getLinks(), "byKey", InternalModel.Addons.class);
        UriBuilder uri = UriBuilder.fromUri(byKeyTemplate.resolve(ImmutableMap.of("addonKey", addonKey)));
        ApiHelper.addAddonQueryParams(query, uri);
        return apiHelper.safeGetOptionalEntity(uri.build(), Addon.class);
    }

    @Override
    public Page<AddonSummary> find(AddonQuery query) throws MpacException
    {
        final UriBuilder uri = fromApiRoot();
        ApiHelper.addAddonQueryParams(query, uri);
        return apiHelper.getMore(new PageReference<>(
            uri.build(), query.getBounds(), pageReader(InternalModel.Addons.class)));
    }

    @Override
    public Addon createAddon(Addon addon) throws MpacException
    {
        // When we query the newly created add-on, we need to add withVersion=true to ensure that you also
        // get the version (since you were required to provide an initial version when you created it)
        Function<URI, URI> resultUriTransform = uri -> UriBuilder.fromUri(uri).queryParam("withVersion", true).build();
        return genericCreate(
                getApiRoot(),
                addon,
                resultUriTransform,
                addDataCenterBuildNumberToHeaders(addon.getDataCenterBuildNumber())
        );
    }

    private Optional<Consumer<HttpPost>> addDataCenterBuildNumberToHeaders(Option<Long> dataCenterBuildNumber) {
        return Optional.of(post ->
                dataCenterBuildNumber.forEach(buildNumber ->
                        post.addHeader(DATA_CENTER_BUILD_NUMBER_HEADER_NAME, Long.toString(buildNumber))
                ));
    }

    @Override
    public Addon updateAddon(Addon original, Addon updated) throws MpacException
    {
        return genericUpdate(original.getSelfUri(), original, updated);
    }

    @Override
    public Optional<AddonVersion> safeGetVersion(String addonKey, AddonVersionSpecifier version, AddonVersionsQuery query) throws MpacException
    {
        AddonQuery queryWithToken = AddonQuery.builder().accessToken(query.safeGetAccessToken()).build();
        for (Addon a: iterableOf(safeGetByKey(addonKey, queryWithToken)))
        {
            UriBuilder uri = UriBuilder.fromUri(getVersionUri(a, version, queryWithToken));
            ApiHelper.addAddonVersionsQueryParams(query, uri);
            return apiHelper.safeGetOptionalEntity(uri.build(), AddonVersion.class);
        }
        return Optional.empty();
    }

    @Override
    public Page<AddonVersionSummary> getVersions(String addonKey, AddonVersionsQuery query) throws MpacException
    {
        for (Addon a: iterableOf(safeGetByKey(addonKey, AddonQuery.builder().accessToken(query.safeGetAccessToken()).build())))
        {
            UriBuilder uri = UriBuilder.fromUri(getVersionsUri(a));
            ApiHelper.addAddonVersionsQueryParams(query, uri);
            return apiHelper.getMore(new PageReference<>(
                uri.build(), query.getBounds(), pageReader(InternalModel.AddonVersions.class)));
        }
        return Page.empty();
    }
    
    @Override
    public AddonVersion createVersion(String addonKey, AddonVersion version) throws MpacException
    {
        for (Addon a: iterableOf(safeGetByKey(addonKey, AddonQuery.any())))
        {
            return genericCreate(getVersionsUri(a), version, addDataCenterBuildNumberToHeaders(version.getDataCenterBuildNumber()));
        }
        throw new MpacException.ServerError(404);
    }

    @Override
    public AddonVersion updateVersion(AddonVersion original, AddonVersion updated) throws MpacException
    {
        return genericUpdate(original.getSelfUri(), original, updated);
    }

    @Override
    public Optional<AddonPricing> safeGetPricing(String addonKey, PricingType pricingType) throws MpacException {
        for (Addon a: iterableOf(safeGetByKey(addonKey, AddonQuery.any())))
        {
            for (URI uri: a.getPricingUri(pricingType))
            {
                return apiHelper.safeGetOptionalEntity(uri, AddonPricing.class);
            }
        }
        return Optional.empty();
    }

    @Override
    public Page<AddonReference> findBanners(AddonQuery query) throws MpacException
    {
        InternalModel.Addons collectionRep = getEmptyBaseCollectionRep();
        final UriBuilder uri = UriBuilder.fromUri(apiHelper.requireLinkUri(collectionRep.getLinks(), "banners", collectionRep.getClass()));
        ApiHelper.addAddonQueryParams(query, uri);
        return apiHelper.getMore(new PageReference<>(uri.build(), query.getBounds(),
            pageReader(InternalModel.AddonReferences.class)));
    }

    @Override
    public Page<AddonReference> findRecommendations(String addonKey, AddonQuery query) throws MpacException
    {
        for (Addon a: iterableOf(safeGetByKey(addonKey, AddonQuery.any())))
        {
            for (URI u: a.getLinks().getUri("recommendations"))
            {
                final UriBuilder uri = UriBuilder.fromUri(u);
                ApiHelper.addAddonQueryParams(query, uri);
                return apiHelper.getMore(new PageReference<>(uri.build(), query.getBounds(),
                    pageReader(InternalModel.AddonReferences.class)));
            }
        }
        return Page.empty();
    }

    @Override
    public boolean claimAccessToken(String addonKey, String token) throws MpacException
    {
        Optional<Addon> addon;
        try
        {
            addon = safeGetByKey(addonKey, AddonQuery.builder().accessToken(Optional.of(token)).build());
        }
        catch (MpacException.ServerError e)
        {
            int status = e.getStatus();
            // UPM-5467 - A 4xx status means that we have an invalid token, so we should not proceed
            // in assuming that the license token is valid (see below)
            if (status >= 400 && status < 500)
            {
                return false;
            }
            throw e;
        }

        for (Addon a: iterableOf(addon))
        {
            for (URI collUri: a.getLinks().getUri("tokens"))
            {
                InternalModel.MinimalLinks rep = apiHelper.getEntity(withZeroLimit(withAccessToken(collUri, token)),
                    InternalModel.MinimalLinks.class);
                UriTemplate t = ApiHelper.requireLinkUriTemplate(rep.getLinks(), "byToken", rep.getClass());
                URI uri = apiHelper.resolveLink(t.resolve(ImmutableMap.of("token", token)));
                SimpleHttpResponse r = apiHelper.getHttp().post(uri, new NullInputStream(0), 0, JSON, JSON, Optional.empty());
                try
                {
                    switch (r.getStatus())
                    {
                        case 200:
                        case 204:
                            return true;
                        case 400:  // request had no Origin
                        case 403:  // token exists, but Origin doesn't match
                        case 404:  // token doesn't exist
                            return false;
                        default:
                            throw apiHelper.responseException(r);
                    }
                }
                finally
                {
                    r.close();
                }
            }
        }
        return false;
    }
    
    private InternalModel.Addons getEmptyBaseCollectionRep() throws MpacException
    {
        return apiHelper.getEntity(withZeroLimit(getApiRoot()), InternalModel.Addons.class);
    }

    private InternalModel.AddonVersions getEmptyVersionCollectionRep(Addon a, AddonQuery query) throws MpacException
    {
        UriBuilder uri = UriBuilder.fromUri(getVersionsUri(a));
        ApiHelper.addAddonQueryParams(query, uri);
        return apiHelper.getEntity(withZeroLimit(uri.build()), InternalModel.AddonVersions.class);
    }
    
    private URI getVersionsUri(Addon a) throws MpacException
    {
        return apiHelper.requireLinkUri(a.getLinks(), "versions", Addon.class);
    }
    
    private URI getVersionUri(Addon a, AddonVersionSpecifier v, AddonQuery query) throws MpacException
    {
        InternalModel.AddonVersions collectionRep = getEmptyVersionCollectionRep(a, query);
        for (Either<String, Long> specifiedVersion: iterableOf(v.getSpecifiedVersion()))
        {
            for (String name: specifiedVersion.left())
            {
                return getTemplatedLink(collectionRep, "byName", "name", name);
            }
            for (Long build: specifiedVersion.right())
            {
                return getTemplatedLink(collectionRep, "byBuild", "pluginBuildNumber", String.valueOf(build));
            }
        }
        return apiHelper.requireLinkUri(collectionRep.getLinks(), "latest", InternalModel.AddonVersions.class);
    }
}
