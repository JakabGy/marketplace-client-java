package com.atlassian.marketplace.client.api;

import org.junit.Test;

import java.util.Optional;

import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.api.Cost.ALL_PAID;
import static com.atlassian.marketplace.client.api.HostingType.CLOUD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ProductQueryTest
{
    @Test
    public void defaultQueryHasNoApplication()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.safeGetApplication(), equalTo(Optional.empty()));
    }
    
    @Test
    public void applicationCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().application(Optional.of(JIRA)).build();
        assertThat(q.safeGetApplication(), equalTo(Optional.of(JIRA)));
    }
    
    @Test
    public void defaultQueryHasNoAppBuildNumber()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.safeGetAppBuildNumber(), equalTo(Optional.empty()));
    }
    
    @Test
    public void appBuildNumberCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().appBuildNumber(Optional.of(1000)).build();
        assertThat(q.safeGetAppBuildNumber(), equalTo(Optional.of(1000)));
    }

    @Test
    public void defaultQueryHasNoCost()
    {
        AddonQuery q = AddonQuery.builder().build();
        assertThat(q.safeGetCost(), equalTo(Optional.empty()));
    }
    
    @Test
    public void costCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().cost(Optional.of(ALL_PAID)).build();
        assertThat(q.safeGetCost(), equalTo(Optional.of(ALL_PAID)));
    }

    @Test
    public void defaultQueryHasNoHosting()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.safeGetHosting(), equalTo(Optional.empty()));
    }
    
    @Test
    public void hostingCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().hosting(Optional.of(CLOUD)).build();
        assertThat(q.safeGetHosting(), equalTo(Optional.of(CLOUD)));
    }
    
    @Test
    public void defaultQueryHasWithVersionFalse()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.isWithVersion(), equalTo(false));
    }
    
    @Test
    public void withVersionCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().withVersion(true).build();
        assertThat(q.isWithVersion(), equalTo(true));
    }

    @Test
    public void defaultQueryHasDefaultBounds()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(Optional.of(3));
        ProductQuery q = ProductQuery.builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
